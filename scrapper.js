const got = require('got');
const jsdom = require("jsdom");

const { JSDOM } = jsdom;
const searchLimit = '10';

var campingList = [];

scrapData();

async function scrapData() {
    let siteList = await getAllLocations();

    console.log(`Found [${siteList.length}] camping sites.`);

    // until logic to limit request is put in place, only work with a few sites
    siteList = siteList.slice(0, 10);
    console.log(`Using [${siteList.length}] camping sites.`);

    // for each camping
    for(let i = 0; i < siteList.length; i++) {        
        // get all the data
        // todo: create logic to limit simultaneous requests to 10
        getCampingData(siteList[i])
        .then(camping => {
            campingList.push(camping);
            console.log(camping);
        });
    }
}

async function getPage(url){
    console.log(`Loading page [${url}]...`);

    try {
        let response = await got(url);
        let dom = new JSDOM(response.body);
        return (require('jquery'))(dom.window);
    } catch (error) {
        console.log(error.response.body);
        return null;
    }
}

async function getCampingData(site){
    console.log(`Getting data for camping [${site.id}]...`)

    try{
        let camping =
        {
            id: site.id,
            url: `https://coolcamping.com/sites/${site.id}`,
            latitude: site.locations[0].latitude,
            longitude: site.locations[0].longitude
        };

        let $ = await getPage(camping.url);

        camping.name = $('[itemprop=name]')[0].content;
        camping.summary = $('.site_summary')[0].textContent;
        
        return camping;
    } catch(error){
        console.log(error.response.body);
        return null;
    }
}

async function getLocation(id) {
    console.log(`Getting location for camping [${id}]...`);

    try {
        let response = await got(`https://coolcamping.com/sites/locations?site_id=${id}`);
        return JSON.parse(response.body).sites[0].locations[0];
    } catch (error) {
        console.log(error.response.body);
        return null;
    }
}

async function getAllLocations(){
    console.log(`Getting all UK camping locations...`);

    try {
        let response = await got(`https://coolcamping.com/sites/locations?site_tag_ids=57`);
        return JSON.parse(response.body).sites;
    } catch (error) {
        console.log(error.response.body);
        return null;
    }
}